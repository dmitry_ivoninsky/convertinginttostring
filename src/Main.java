import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        StringToInt s = new StringToInt();
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        try {
            s.stringToInt(str);
        }
        catch (SpecialCharException e) {
            System.out.println(e.getMessage() + ", попробуйте снова.");
        }

    }

}
